#include "uart.hpp"
#include <stdlib.h>  // NULL
#include <avr/interrupt.h>
 
UART::UART()
    : txd("D1w")
    , rxd("D0r")
{ 
    // PD1: TxD, PD0: RxD
    UCSR0A |= (1<<U2X0);
    UCSR0B |= (1<<RXEN0)  | (1<<TXEN0);
    UCSR0C |= (1<<UCSZ00) | (1<<UCSZ01);

    UBRR0 =  16; // 115200
}

UART::~UART()
{
}

//////////////////////////////////////////////////////////////////////////////

size_t UART::ReadByte(uint8_t *byte)
{  
    // wait until a byte is ready to read
    while( ( UCSR0A & ( 1 << RXC0 ) ) == 0 );
    // grab the byte from the serial port
    if(byte != NULL)
    {
        *byte = UDR0;
        return 1;
    }

    return 0;
}

static uint8_t rxbuffer;
static bool    rxbuffervalid = false;

size_t UART::AsyncReadByte(uint8_t *byte)
{
    // is something in the buffer?
    if(rxbuffervalid)
    {
        if(byte)
            *byte = rxbuffer;
        rxbuffervalid = false;
        return 1;
    }

    // is something in the register?
    if((UCSR0A & (1<<RXC0)) != 0)
    {
        if(byte)
            *byte = UDR0;
        return 1;
    }

    // activate ints if not already done
    if( !(UCSR0B & (1<<RXCIE0)) )
        UCSR0B |= (1<<RXCIE0);
    return 0;
}

ISR(USART_RX_vect)
{
    rxbuffer      = UDR0;
    rxbuffervalid = true;
    UCSR0B       &= ~(1<<RXCIE0); // disable int.
}

//////////////////////////////////////////////////////////////////////////////

size_t UART::WriteByte(uint8_t byte)
{  
    // wait until the port is ready to be written to
    while( ( UCSR0A & ( 1 << UDRE0 ) ) == 0 );
    // write the byte to the serial port
    UDR0 = byte;

    return 1;
}

size_t UART::AsyncWriteByte(uint8_t byte)
{  
    if((UCSR0A & (1 << UDRE0)) != 0)
    {
        UDR0 = byte;
        return 1;
    }
    return 0;
}

//////////////////////////////////////////////////////////////////////////////

void UART::Write(const char *string)
{
    if(string == NULL)
        return;

    while(*string)
    {
        this->WriteByte((uint8_t)*string);
        string++;
    }
}

// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 
