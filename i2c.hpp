#ifndef I2C_HPP
#define I2C_HPP

#include <avr/io.h>
#include <stdint.h>
#include <stdlib.h>
#include <pin.hpp>

class I2C
{
    public:
        I2C();
        ~I2C();

        void Write(uint8_t address, const uint8_t buffer[], size_t length);
        void Read (uint8_t address,       uint8_t buffer[], size_t length);
        void WriteRead(uint8_t address, const uint8_t writebuffer[], size_t wblength,
                                              uint8_t readbuffer[],  size_t rblength);

        void ConnectStatusLED(PIN &led);

    private:
        PIN sda, scl;
        PIN *statusled;

        void SetStartBit();
        void SetStopBit();
        bool SetData(uint8_t data);
        uint8_t GetData(bool ack = true);
};

#endif

// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

