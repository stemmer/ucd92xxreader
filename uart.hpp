#ifndef UART_HPP
#define UART_HPP

#include <avr/io.h>
#include <stdint.h>
#include <stdlib.h>
#include <pin.hpp>
 
#define USART_BAUDRATE 115200
#define BAUD_PRESCALE (((F_CPU/(USART_BAUDRATE*16UL)))-1)

class UART
{
 public:
    UART();
    ~UART();

    size_t ReadByte(uint8_t *byte);
    size_t AsyncReadByte(uint8_t *byte);
    size_t WriteByte(uint8_t byte);
    size_t AsyncWriteByte(uint8_t byte);

    void Write(const char *string);

 private:
    PIN txd, rxd;
};


#endif
// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 
