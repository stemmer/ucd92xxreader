#include <i2c.hpp>
#include <stdlib.h>  // NULL
#include <util/twi.h>


I2C::I2C()
    : sda("C4r")
    , scl("C5r")
    , statusled(NULL)
{
    TWSR = 0; // prescale = 0
    TWBR = ((F_CPU/100000)-16)/2;
    TWCR = (1 << TWEN);
}

I2C::~I2C()
{
    TWCR &= ~(1 << TWEN);
}


void I2C::ConnectStatusLED(PIN &led)
{
    this->statusled = &led;
}


void I2C::SetStartBit()
{
    TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
    while(!(TWCR & (1 << TWINT)));

    return;
}

void I2C::SetStopBit()
{
    TWCR = (1 << TWINT) | (1 << TWSTO) | (1 << TWEN);

    return;
}

bool I2C::SetData(uint8_t data)
{
    TWDR = data;
    TWCR = (1 << TWINT) | (1<<TWEN);
    while(!(TWCR & (1 << TWINT)));

    if ((TWSR & 0xF8) != TW_MT_DATA_ACK)
        return false;

    return true;
}

uint8_t I2C::GetData(bool ack /*=true*/)
{
    TWCR = (1 << TWINT) | (1 << TWEN);
    if(ack)
        TWCR |= (1 << TWEA);

    while(!(TWCR & (1 << TWINT)));
    return TWDR;
}



void I2C::Write(uint8_t address, const uint8_t buffer[], size_t length)
{
    if(this->statusled != NULL)
        this->statusled->Toggle();

    this->SetStartBit();
    this->SetData((address<<1) | 0);
    for(size_t i=0; i<length; i++)
        this->SetData(buffer[i]);
    this->SetStopBit();

    if(this->statusled != NULL)
        this->statusled->Toggle();
}



void I2C::Read(uint8_t address, uint8_t buffer[], size_t length)
{
    if(this->statusled != NULL)
        this->statusled->Toggle();

    this->SetStartBit();
    this->SetData((address<<1) | 1);

    bool ack = true;
    for(size_t i=0; i<length; i++)
    {
        if(i == length-1)
            ack = false;    // Send NACK for last byte
        buffer[i] = this->GetData(ack);
    }

    this->SetStopBit();

    if(this->statusled != NULL)
        this->statusled->Toggle();
}



void I2C::WriteRead(uint8_t address, const uint8_t writebuffer[], size_t wblength,
                                           uint8_t readbuffer[],  size_t rblength)
{
    if(this->statusled != NULL)
        this->statusled->Toggle();

    this->SetStartBit();
    
    this->SetData((address<<1) | 0);
    for(size_t i=0; i<wblength; i++)
    {
        this->SetData(writebuffer[i]);
    }

    this->SetStartBit(); // Set Restart bit
    
    this->SetData((address<<1) | 1);
    bool ack = true;
    for(size_t i=0; i<rblength; i++)
    {
        if(i == rblength-1)
            ack = false;    // Send NACK for last byte
        readbuffer[i] = this->GetData(ack);
    }
    
    this->SetStopBit();

    if(this->statusled != NULL)
        this->statusled->Toggle();
}


// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

