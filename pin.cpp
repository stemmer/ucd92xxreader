#include "pin.hpp"
#include <stdlib.h>  // NULL
#include <avr/io.h>

PIN::PIN(const char ID[4])
{
    this->port   = NULL;
    this->inport = NULL;
    this->ddr    = NULL;
    this->pin    = -1;
    this->isoutput = false;

    if(ID != NULL)
    {
        // Decode port
        switch(ID[0])
        {
            case 'B':
            case 'b':
                this->port   = &PORTB;
                this->inport = &PINB;
                this->ddr    = &DDRB;
                break;
            case 'C':
            case 'c':
                this->port   = &PORTC;
                this->inport = &PINC;
                this->ddr    = &DDRC;
                break;
            case 'D':
            case 'd':
                this->port   = &PORTD;
                this->inport = &PIND;
                this->ddr    = &DDRD;
                break;
        }

        // Decode pin
        int pin;
        pin = ID[1] - '0';
        if(pin >= 0 && pin <= 7)
            this->pin = pin;

        // Decode direction
        this->SetDirection(ID[2]);
    }   

}

PIN::~PIN()
{
}

void PIN::SetDirection(const char dir)
{
    switch(dir)
    {
        case 'o':
        case '>':
        case 'w':
            this->isoutput = true;
            break;
        case 'i':
        case '<':
        case 'r':
            this->isoutput = false;
            break;
        default:
            return; // noting to do if unknown char
    }

    if(this->ddr != NULL && this->pin != -1)
    {
        // set direction in controller - port and pin are always valid
        *(this->ddr) |=  ( this->isoutput << this->pin);
        *(this->ddr) &= ~(!this->isoutput << this->pin);
    }
}

bool PIN::Get(void)
{
    if(this->port == NULL || this->pin == -1)
        return 0;

    return (*(this->inport) >> this->pin) & 1;
}

void PIN::Set(bool bit)
{
    if(this->port == NULL || this->pin == -1)
        return;

    if(bit)
        *(this->port) |=  (1 << this->pin);
    else
        *(this->port) &= ~(1 << this->pin);
}

void PIN::Toggle()
{
    if(this->Get() == true)
        this->Set(false);
    else
        this->Set(true);
}

// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 
