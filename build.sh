#!/usr/bin/env bash

OPT="-O2"
CPP_FLAGS="$OPT -DF_CPU=16000000UL -mmcu=atmega328p -I. -c"

CPP_SOURCE=`find . -name "*.cpp"`
for i in $CPP_SOURCE
do
    echo -e "\e[1;36m - \e[1;34m$i\e[0m"
    avr-g++ $CPP_FLAGS -o ${i%.*}.o $i
    if [[ $? != 0 ]] ; then
        exit 1
    fi
done

O_SOURCE=`find . -name "*.o"`
avr-g++ -mmcu=atmega328p -o binary.elf $O_SOURCE

chmod -x binary.elf
avr-objcopy -O ihex -R .eeprom binary.elf binary.hex

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 
