#ifndef UCD92xxRail_HPP
#define UCD92xxRail_HPP

#include <i2cdev/UCD92xx.hpp>
#include <stdint.h>
#include <stdlib.h>


class UCD92xxRail
{
    public:
        UCD92xxRail(UCD92xx &chip, uint8_t page);
        ~UCD92xxRail();

        double GetVoltage();
        double GetCurrent();


    private:
        uint8_t  page;
        UCD92xx *chip;
};

#endif

// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

