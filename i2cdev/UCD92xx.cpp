#include <i2cdev/UCD92xx.hpp>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

UCD92xx::UCD92xx(uint8_t address)
    : I2CDevice(address)
    , uart(NULL)
{}

UCD92xx::~UCD92xx()
{}

UCD92xx::UCD92xx(const UCD92xx &source)
    : I2CDevice(source)
{}


void UCD92xx::Debug(UART &uart)
{
    this->uart = &uart;
}


void UCD92xx::UpdatePhaseMapping()
{
    if(this->uart)
        this->uart->Write("\e[1;30mReading phase mapping: \n");

    uint8_t readmapping[4];
    uint8_t command[1] = {CMD_PHASE_INFO};
    this->I2CDevice::WriteRead(command, 1, readmapping, 4);

    if(this->uart)
    {
        this->uart->Write("\e[1;30mPG PH\n");
        for(int i=0; i<4; i++)
        {
            char buffer[16];
            snprintf(buffer, sizeof(buffer), "\e[1;30m%d: 0x%2X\n\e[0m",
                    i, readmapping[i]);
            this->uart->Write(buffer);
        }
    }

    // Actually update the mapping
    for(int page = 0; page < 4; page++)
    {
        uint8_t phase;
        uint8_t code;

        // Translate read phase mapping. Each phase has a channel A and B.
        code = readmapping[page];
        if(code & 0x03)
            phase = 0;
        else if(code & 0x0C)
            phase = 1;
        else if(code & 0x30)
            phase = 2;
        else if(code & 0xC0)
            phase = 3;
        else      
            phase = ALL_PHASES;

        this->phasemapping[page] = phase;
    }
}


double UCD92xx::GetVoltage(uint8_t page)
{
    if(this->uart)
        this->uart->Write("\e[1;30mGetVoltage: ");

    uint16_t voltagecode;
    this->SetPage(page);
    voltagecode = this->GetData(CMD_READ_VOUT);

    if(this->uart)
    {
        char buffer[16];
        snprintf(buffer, sizeof(buffer), "0x%X\e[0m\n", voltagecode);
        this->uart->Write(buffer);
    }

    // UCD92xx uses an exponent of always -12 ( -> 0.244mV steps)
    // mantissa · 2^exponent
    double voltage;
    voltage = voltagecode * 0.000244L;
    return voltage;
}

double UCD92xx::GetCurrent(uint8_t page)
{
    if(this->uart)
        this->uart->Write("\e[1;30mGetCurrent: ");

    uint16_t currentcode;
    this->SetPage(page);
    this->SetPhase(this->phasemapping[page]);
    currentcode = this->GetData(CMD_READ_IOUT);

    if(this->uart)
    {
        char buffer[16];
        snprintf(buffer, sizeof(buffer), "0x%X\e[0m\n", currentcode);
        this->uart->Write(buffer);
    }

    // Spec: PMBus
    // mantissa · 2^exponent
	signed char  exponent;
	signed short mantissa;

    // Exponent: 5bit 2s-complement
	exponent = currentcode >> 11;
    if(exponent > 0x0f)
		exponent |= 0xe0; // Extend sign-bits for negative values

    // Mantissa: 11bit 2s-complement
	mantissa = currentcode & 0x7ff;
	if(mantissa > 0x03ff)
		mantissa |= 0xf800; // Extend sign-bits for negative values

    double current;
    current = mantissa * pow(2.0, exponent);
    return current;
}



uint16_t UCD92xx::GetData(uint8_t reg)
{
    uint8_t command[1];
    uint8_t response[2];
    
    command[0] = reg;

    this->I2CDevice::WriteRead(command, 1, response, 2);

    uint16_t code;
    code = ((uint16_t)response[1] << 8) | (uint16_t)response[0];
    return code;
}



void UCD92xx::SetPage(uint8_t page)
{
    uint8_t command[2];
    command[0] = CMD_PAGE;
    command[1] = page;
    this->I2CDevice::Write(command, sizeof(command));
}



void UCD92xx::SetPhase(uint8_t phase)
{
    uint8_t command[2];
    command[0] = CMD_PHASE;
    command[1] = phase;
    this->I2CDevice::Write(command, sizeof(command));
}


// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

