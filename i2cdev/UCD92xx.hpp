#ifndef UCD92xx_HPP
#define UCD92xx_HPP

#include <i2cdev/i2cdevice.hpp>
#include <stdint.h>
#include <stdlib.h>
#include <uart.hpp>


class UCD92xx : public I2CDevice
{
    public:
        UCD92xx(uint8_t address = 0x00);
        ~UCD92xx();
        UCD92xx(const UCD92xx &source);

        void UpdatePhaseMapping();

        double GetVoltage(uint8_t page);
        double GetCurrent(uint8_t page);

        void Debug(UART &uart);

    private:
        UART *uart; // For debugging when != NULL

        const uint8_t CMD_PAGE       = 0x00;
        const uint8_t CMD_PHASE      = 0x04;
        const uint8_t CMD_READ_VOUT  = 0x8B;
        const uint8_t CMD_READ_IOUT  = 0x8C;
        const uint8_t CMD_PHASE_INFO = 0xD2;

        // Maps the phase of each page:
        // phase = phasemapping[page];
        const uint8_t ALL_PHASES    = 0xFF;
        uint8_t phasemapping[4] = {ALL_PHASES, ALL_PHASES, ALL_PHASES, ALL_PHASES};

        uint16_t GetData(uint8_t reg);
        void SetPage(uint8_t page);
        void SetPhase(uint8_t page);

};

#endif

// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

