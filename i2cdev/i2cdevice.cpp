#include <i2cdev/i2cdevice.hpp>

I2CDevice::I2CDevice(uint8_t address)
    : address(address)
    , bus(NULL)
{
}

I2CDevice::~I2CDevice()
{
}

I2CDevice::I2CDevice(const I2CDevice &source)
    : address(source.address)
    , bus(source.bus)
{}


void I2CDevice::Connect(I2C &bus)
{
    this->bus = &bus;
}

I2CDevice& I2CDevice::operator>> (I2C &bus)
{
    this->Connect(bus);
    return *this;
}



void I2CDevice::Write(const uint8_t buffer[], size_t length)
{
    if(this->bus == NULL)
        return;

    this->bus->Write(this->address, buffer, length);
    return;
}



void I2CDevice::Read(uint8_t buffer[], size_t length)
{
    if(this->bus == NULL)
        return;

    this->bus->Read(this->address, buffer, length);
    return;
}



void I2CDevice::WriteRead(const uint8_t writebuffer[], size_t wblength,
                                uint8_t readbuffer[],  size_t rblength)
{
    if(this->bus == NULL)
        return;

    this->bus->WriteRead(this->address, writebuffer, wblength, readbuffer, rblength);
}

// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

