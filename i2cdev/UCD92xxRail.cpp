#include <i2cdev/UCD92xxRail.hpp>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

UCD92xxRail::UCD92xxRail(UCD92xx &chip, uint8_t page)
    : chip(&chip)
    , page(page)
{}

UCD92xxRail::~UCD92xxRail()
{}

double UCD92xxRail::GetVoltage()
{
    return this->chip->GetVoltage(this->page);
}

double UCD92xxRail::GetCurrent()
{
    return this->chip->GetCurrent(this->page);
}


// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

