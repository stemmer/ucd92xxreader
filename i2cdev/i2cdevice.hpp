#ifndef I2CDEVICE_HPP
#define I2CDEVICE_HPP

#include <i2c.hpp>
#include <stdint.h>
#include <stdlib.h>

class I2CDevice
{
    public:
        I2CDevice(uint8_t address);
        ~I2CDevice();
        I2CDevice(const I2CDevice &source);

        void Connect(I2C &bus);
        I2CDevice& operator>> (I2C &bus);

        void Write(const uint8_t buffer[], size_t length);
        void Read (      uint8_t buffer[], size_t length);
        void WriteRead(const uint8_t writebuffer[], size_t wblength,
                             uint8_t readbuffer[],  size_t rblength);

    private:
        uint8_t address;
        I2C *bus;
};

#endif

// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

