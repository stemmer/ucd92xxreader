# UCD92xxReader

A small Arduino project to read voltage and current from the Intel UCD92xx power management chips of an Xilinx ZC702 board.

## Requirements

  -  A Linux system
  -  _avr-gcc_ package
  -  _avrdude_ package
  -  A UART terminal like _sterm_: https://github.com/rstemmer/sterm
  -  An Arduino¹ board
  -  A cable to connect the PMBus (I²C) interface from the ZC702 with the Arduino
  -  A logic level converter to convert between the 3.3V of the ZC702 and the 5V of the Arduino board.

¹ Any other AVR based board can be used. Just need to edit the pin configurations in uart.cpp, i2c.cpp and main.cpp

