#include <math.h>
#include <uart.hpp>
#include <i2c.hpp>
#include <i2cdev/UCD92xx.hpp>
#include <i2cdev/UCD92xxRail.hpp>
#include <pin.hpp>
#include <avr/interrupt.h>
#include <stdio.h>

#define DEV_U32_ADDRESS     52
#define DEV_U33_ADDRESS     53
#define DEV_U34_ADDRESS     54

#define U32_PAGE_VCCINT     0
#define U32_PAGE_VCCPINT    1
#define U32_PAGE_VCCAUX     2
#define U32_PAGE_VCCPAUX    3

#define U33_PAGE_VADJ       0
#define U33_PAGE_VCC1V5_PS  1
#define U33_PAGE_VCCMIO_PS  2
#define U33_PAGE_VCCBRAM    3

#define U34_PAGE_VCC3V3     0
#define U34_PAGE_VCC2V5     1

void PrintData(UART &uart, const char *name, double voltage, double current);

int main(void)
{
    UART uart;
    I2C  i2c;
    PIN  led("B5w"); // output for LED at PB5 for I²C status
    led.Set(false);  // Turn off LED

    i2c.ConnectStatusLED(led);

    UCD92xx ChipU32(DEV_U32_ADDRESS);
    UCD92xx ChipU33(DEV_U33_ADDRESS);
    UCD92xx ChipU34(DEV_U34_ADDRESS);

    ChipU32 >> i2c;
    ChipU33 >> i2c;
    ChipU34 >> i2c;

    //ChipU32.Debug(uart);
    //ChipU33.Debug(uart);
    ChipU32.UpdatePhaseMapping();
    ChipU33.UpdatePhaseMapping();

    UCD92xxRail PL  (ChipU32, U32_PAGE_VCCINT);    // 52.0 (1.0V)
    UCD92xxRail PS  (ChipU32, U32_PAGE_VCCPINT);   // 52.1 (1.0V)
    UCD92xxRail DDR (ChipU33, U33_PAGE_VCC1V5_PS); // 53.1 (1.5V)
    UCD92xxRail BRAM(ChipU33, U33_PAGE_VCCBRAM);   // 53.3 (1.0V)

    sei();

    while(true)
    {
        char buffer[128];
        snprintf(buffer, sizeof(buffer), "\e[1;36m Name \e[1;35m| \e[1;36m Voltage  \e[1;35m| \e[1;36m Current\n");
        uart.Write(buffer);
        snprintf(buffer, sizeof(buffer), "\e[1;35m------+-----------+-----------\n");
        uart.Write(buffer);

        double current = 0.0;
        double voltage = 0.0;
        
        current = PS.GetCurrent();
        voltage = PS.GetVoltage();
        PrintData(uart, "PS  ", voltage, current);

        current = DDR.GetCurrent();
        voltage = DDR.GetVoltage();
        PrintData(uart, "DDR ", voltage, current);

        current = PL.GetCurrent();
        voltage = PL.GetVoltage();
        PrintData(uart, "PL  ", voltage, current);

        current = BRAM.GetCurrent();
        voltage = BRAM.GetVoltage();
        PrintData(uart, "BRAM", voltage, current);
        uart.Write("\e[0m\n");

        for(volatile uint32_t i = 0; i < 300000; i++); // wait
    }
    return 0;   /* never reached */
}


void PrintData(UART &uart, const char *name, double voltage, double current)
{
    long int v;
    long int i;
    char buffer[128];

    v = (long int)(voltage * 1000 * 1000);
    i = (long int)(current * 1000 * 1000);

    snprintf(buffer, sizeof(buffer), " \e[1;36m%s \e[1;35m| \e[1;34m%7ldµV \e[1;35m| \e[1;34m%7ldµA\n", name, v, i);
    uart.Write(buffer);
}


// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 
