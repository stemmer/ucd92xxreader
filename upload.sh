#!/usr/bin/env bash

avrdude -F -V -c arduino -p ATMEGA328P -P $1 -b 115200 -U flash:w:binary.hex

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 
