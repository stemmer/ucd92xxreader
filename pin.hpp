#ifndef PIN_HPP
#define PIN_HPP
#include <avr/io.h>
#include <stdint.h>
 
class PIN
{
 public:
    PIN(const char ID[4]); // "A2<" <: in; >: out (oder r/w, i/o)
    ~PIN();

    void SetDirection(const char dir); // r/w </> i/o
    bool Get(void);
    void Set(bool bit);
    void Toggle();

 private:
    volatile uint8_t *port;
    volatile uint8_t *inport;
    volatile uint8_t *ddr;
    int pin;
    bool isoutput;
};

#endif
// vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4 
